
#define GLM_ENABLE_EXPERIMENTAL

//#include <Afx.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
/*#include "Vector3D.hpp"
#include "Matrix3D.h"
#include "BoundingBox.h"*/

#include <Mesh.hpp>
#include <Application.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>
#include <LightInfo.hpp>

#include <random>
#include <iostream>
#include <vector>
#include <string>
#include <stack>
#include <map>

using namespace std;

float frand()
{
	return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
}

struct State
{
	glm::vec3 pos;
	glm::quat curRot;
	glm::vec3 angles;
	float curDist;
	float curRad;
	float curDistScale;
	bool inverted;
	int treeLevel;
	bool isLeaf;
};

class LSystem
{
private:

	map <char, string> rules;
	string             initialString;
	glm::vec3          angles;
	float              distScale;
	float              angleScale;
	string             currentString;
	float			   initDist;
	float			   initRad;
	float			   branchDist;
	float              branchRad;
	int				   leafLevel;
	int				   branchingLevel;
	int				   numBranches;
	glm::vec3          initDir = glm::vec3(0.0f, 0.0f, 1.0f);

public:

	void setInitDist(const float newDist) {
		initDist = newDist;
	}

	void setLeafLevel(const float numbOfIter) {
		leafLevel = numbOfIter;
	}

	void setNumBranches(const float numBr) {
		numBranches = numBr;
	}

	void setBranchingLevel(const float brLv) {
		branchingLevel = brLv;
	}

	void setInitRad(const float newRad) {
		initRad = newRad;
	}

	void setInitialString(const char * str)
	{
		initialString = str;
	}

	void addRule(char symbol, const char * rule)
	{
		rules[symbol] = rule;
	}

	void setAngle(glm::vec3 newAngles)
	{
		angles = newAngles;
	}

	const string& getCurrentString() const
	{
		return currentString;
	}

	void setDistScale(float newScale)
	{
		distScale = newScale;
	}

	void setAngleScale(float scale)
	{
		angleScale = scale;
	}

	void buildSystem(int numIterations) {

		currentString = initialString;

		branchDist = initDist;
		branchRad = initRad;

		for (int i = 0; i < numIterations; i++)
		{
			currentString = oneStep(currentString);
			branchDist *= distScale;
			branchRad *= distScale;
		}
	}

	State getStartingState() {
		State startingState;
		startingState.pos = glm::vec3(0.0f, 0.0f, 0.0f);
		startingState.curRot = glm::angleAxis(0.0f, glm::vec3(0.0f, 0.0f, 1.0f));
		startingState.curDist = initDist;
		startingState.curRad = initRad;
		startingState.curDistScale = distScale;
		startingState.treeLevel = 1;
		startingState.isLeaf = false;
		return startingState;
	}

	float getDistanceScale() {
		return distScale;
	}

	State getBranch() {
		State branch;
		branch.curDist = branchDist;
		branch.curRad = branchRad;
		branch.curDistScale = 0.1f;
		branch.treeLevel = leafLevel;
		branch.isLeaf = false;
		return branch;
	}

	//��������� ��������� ��������� ����� ��� ������ �� ����� � �������
	void makeBranches(vector<State>& resStates, State& currentState) {
		glm::vec3 branchStep = currentState.curRot * initDir * currentState.curDist / (float)(numBranches+3);
		State branch = getBranch();
		glm::quat rotQuat;

		std::random_device rd;
		std::mt19937 rng(rd());
		std::uniform_int_distribution<int> uni(0, 5);

		int curRand = uni(rng);
		int prevRand = curRand;

		branch.pos = currentState.pos;
		branch.curRot = currentState.curRot;

		for (int i = 0; i < numBranches; ++i) {
			branch.pos = currentState.pos + branchStep * (float)(i+2);
			branch.isLeaf = false;
			do {
				while (curRand == prevRand) {
					curRand = uni(rng);
				}
				switch (curRand) {
				case 0:
					rotQuat = glm::angleAxis(angles[0] * 1.5f, glm::vec3(1.0f, 0.0f, 0.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				case 1:
					rotQuat = glm::angleAxis(-1.5f * angles[0], glm::vec3(1.0f, 0.0f, 0.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				case 2:
					rotQuat = glm::angleAxis(angles[1] * 1.5f, glm::vec3(0.0f, 1.0f, 0.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				case 3:
					rotQuat = glm::angleAxis(-1.5f * angles[1], glm::vec3(0.0f, 1.0f, 0.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				case 4:
					rotQuat = glm::angleAxis(angles[2] * 1.5f, glm::vec3(0.0f, 0.0f, 1.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				case 5:
					rotQuat = glm::angleAxis(-1.5f * angles[2], glm::vec3(0.0f, 0.0f, 1.0f));
					branch.curRot = currentState.curRot * rotQuat;
					prevRand = curRand;
					break;
				default:
					std::cout << "WHOOOOOPS" << curRand << endl;
					break;
				}
				//std::cout << glm::dot(currentState.curRot * initDir, branch.curRot * initDir) << std::endl;
			} while (glm::dot(currentState.curRot * initDir, branch.curRot * initDir) > 0.999);
			branch.pos += branch.curRot * initDir * branch.curDist * 0.5f;
			resStates.push_back(branch);
			branch.pos += branch.curRot * initDir * branch.curDist * 0.5f;
			branch.isLeaf = true;
			resStates.push_back(branch);
		}
	}
	//�������� ������� ������ �� ������ � ����������� ���������
	//[ � ] -- ���������� � ����� �� �����
	// F -- �������� ����� � ������� �����
	// | -- ��������� �����������
	// + � - -- ������� ������ (1,0,0)
	// > � < -- ������� ������ (0,1,0)
	// / � \ -- ������� ������ (0,0,1)
	vector<State> makeCarcas() {
		std::stack<State> stateStack;
		State currentState = getStartingState();
		vector<State> resStates;
		int currentLevel = 0;
		for (int i = 0; i < currentString.length(); ++i) {
			switch (currentString[i]) {
			case '[':
				stateStack.push(currentState);
				break;
			case ']':
				currentState = stateStack.top();
				stateStack.pop();
				break;
			case 'F':
				if (currentState.treeLevel < leafLevel) {
					if (currentState.treeLevel >= branchingLevel && currentState.treeLevel < (leafLevel)) {
						makeBranches(resStates, currentState);
					}
					currentState.pos += currentState.curRot * initDir * currentState.curDist * 0.5f;
					resStates.push_back(currentState);
					currentState.pos += currentState.curRot * initDir * currentState.curDist * 0.5f;
					currentState.curDist *= distScale;
					currentState.curRad *= distScale;
					currentState.treeLevel += 1;
				}
				else if (currentState.treeLevel == leafLevel) {
					currentState.curDistScale = 0.1f;
					currentState.pos += currentState.curRot * initDir * currentState.curDist * 0.5f;
					resStates.push_back(currentState);
					currentState.pos += currentState.curRot * initDir * currentState.curDist * 0.5f;
					currentState.curDist *= distScale;
					currentState.curRad *= distScale;
					currentState.treeLevel += 1;
					currentState.isLeaf = true;
					resStates.push_back(currentState);
				}
				break;
			case '|':
				if(currentState.inverted) {
					currentState.inverted = false;
				}
				else {
					currentState.inverted = true;
				}
				break;
			case '+':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[0], glm::vec3(1.0f, 0.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis(angles[0], glm::vec3(1.0f, 0.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			case '-':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis(angles[0], glm::vec3(1.0f, 0.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[0], glm::vec3(1.0f, 0.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			case '>':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[1], glm::vec3(0.0f, 1.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis(angles[1], glm::vec3(0.0f, 1.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			case '<':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis(angles[1], glm::vec3(0.0f, 1.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[1], glm::vec3(0.0f, 1.0f, 0.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			case '/':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[2], glm::vec3(0.0f, 0.0f, 1.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis(angles[2], glm::vec3(0.0f, 0.0f, 1.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			case '\\':
				if (currentState.inverted) {
					glm::quat rotQuat = glm::angleAxis(angles[2], glm::vec3(0.0f, 0.0f, 1.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				else {
					glm::quat rotQuat = glm::angleAxis((float)-1.0 * angles[2], glm::vec3(0.0f, 0.0f, 1.0f));
					currentState.curRot = currentState.curRot * rotQuat;
				}
				break;
			default:
				break;
			}

		}
		return resStates;
	}

protected:
	string   oneStep(const string& in) const {
		string out;
		map <char, string> ::const_iterator it;

		for (int i = 0; i < in.length(); i++)
		{
			it = rules.find(in[i]);

			if (it != rules.end())
				out += it->second;
			else
				out += in[i];
		}

		return out;
	}
};

/**
������, ������, ������������ �������� �����, ������
*/
class TestingApplication : public Application
{
public:
	vector<vector<MeshPtr>> _cylinders;
	vector<MeshPtr> _leaves;
	vector<int> _cylinders_resolution{ 5, 15, 40 };
	
	vector<glm::vec3> _positionsVec3;
	vector<glm::vec3> _positionsHouses;

	glm::vec3 _boundingRect;

	MeshPtr _house;
	MeshPtr _ground;

	ShaderProgramPtr _shader;
	ShaderProgramPtr _leafShader;
	ShaderProgramPtr _groundShader;
	ShaderProgramPtr _cullingShader;
	ShaderProgramPtr _hizShader;

	//���������� ��������� �����
	float _phi = 0.0f;
	float _theta = glm::pi<float>() * 0.25f;

	int _num_of_houses = 5;
	int _num_of_trees = 300;
	int _treeResolution = 0;
	int _treePolygons = 0;
	int _unoccludedTrees[3] = { 0, 0, 0 };

	TexturePtr _barkTexture;
	TexturePtr _normalTexture;
	TexturePtr _leafTexture;
	TexturePtr _grassTexture;
	TexturePtr _brickTexture;
	TexturePtr _bufferTex[3];
	GLuint _depthTex;

	DataBufferPtr _bufVec4;
	DataBufferPtr _bufVec3;

	GLuint _sampler;
	GLuint treeVertexArray;
	GLuint _transformFeedback;
	GLuint _culledVbo[3];
	GLuint _precullVao;
	GLuint treeTBO;
	GLuint culledTreeTBO;
	GLuint treeBTex;
	GLuint _culledTreeQueries[3];
	GLuint _hizFramebuffer;

	bool _hizBuffersSetUp = false;

	MeshPtr _positions;

	LightInfo _light;

	void makeScene() override
	{
		Application::makeScene();

		//=========================================================
		//�������� � �������� �����	

		LSystem ls;
		ls.setInitialString("S");
		ls.setAngle(glm::vec3(0.4f, 0.4f, 0.4f));
		ls.setDistScale(0.75f);
		ls.setInitDist(0.9f);
		ls.setInitRad(0.1f);
		ls.addRule('S', "F[AB]");
		ls.addRule('A', "[<|FC+E]");
		ls.addRule('B', "[>|FD-G]");
		ls.addRule('C', "[-|FE/B]");
		ls.addRule('D', "[+|FG\\A]");
		ls.addRule('E', "[/|FC>B]");
		ls.addRule('G', "[\\|FD<A]");
		ls.buildSystem(7);
		ls.setLeafLevel(6);
		ls.setBranchingLevel(3);
		ls.setNumBranches(2);
		vector<State> states = ls.makeCarcas();

		_boundingRect = glm::vec3(0.85f, 0.85f, 0.85f);

		//������������ ���������� �������� � ���������� ������
		GLint maxTextureBufferSize;
		glGetIntegerv(GL_MAX_TEXTURE_BUFFER_SIZE, &maxTextureBufferSize);

		std::cout << "GL_MAX_TEXTURE_BUFFER_SIZE " << maxTextureBufferSize << std::endl;

		glGetIntegerv(GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS, &maxTextureBufferSize);

		std::cout << "GL_MAX_TRANSFORM_FEEDBACK_INTERLEAVED_COMPONENTS " << maxTextureBufferSize << std::endl;

		float _size = 30.0f;

		srand((int)(glfwGetTime() * 1000));

		//_positionsVec3.push_back(glm::vec3(5.0, 0.0, 1.0));

		for (int i = 0; i < _num_of_houses; i++)
		{
			_positionsHouses.push_back(glm::vec3(frand() * _size - 0.5 * _size, frand() * _size - 0.5 * _size, 0.0));
			//std::cout << _positionsVec3[_positionsVec3.size() - 1][0] << "  " << _positionsVec3[_positionsVec3.size() - 1][1] << std::endl;
		}

		_size = 30.0f;

		for (int i = 0; i < _num_of_trees; i++)
		{
			_positionsVec3.push_back(glm::vec3(frand() * _size - 0.5 * _size, frand() * _size - 0.5 * _size, 0.0));
			//std::cout << _positionsVec3[_positionsVec3.size() - 1][0] << "  " << _positionsVec3[_positionsVec3.size() - 1][1] << std::endl;
		}

		std::cout << _positionsVec3.size() << std::endl;

		_cylinders.resize(3);
		
		for (int i = 0; i < states.size(); ++i) {
			if (states[i].isLeaf) {
				_leaves.push_back(makeRectangle(states[i].curDist, states[i].curRad * 4.0f));
				_leaves[_leaves.size() - 1]->setModelMatrix(glm::translate(glm::mat4(1.0f), states[i].pos) *  glm::toMat4(states[i].curRot));
			}
			else {
				for (int j = 0; j < 3; ++j) {
					_cylinders[j].push_back(makeCylinder(states[i].curDist, states[i].curRad, _cylinders_resolution[j], _cylinders_resolution[j], states[i].curDistScale));
					_cylinders[j][_cylinders[j].size() - 1]->setModelMatrix(glm::translate(glm::mat4(1.0f), states[i].pos) * glm::toMat4(states[i].curRot));
				}
			}
		}

		_house = makeCube(1.5f);
		_house->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 1.5f)));

		_ground = makeGroundPlane(20, 10);
		_ground->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0, 0.0, -0.05)));

		_bufVec3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
		_bufVec3->setData(_positionsVec3.size() * sizeof(float) * 3, _positionsVec3.data());
		
		//----------------------------
		//�������������� �����, � ������� ����� ����������� �������� ������� � ��������� ����������� ����� ���������
		for (int i = 0; i < 3; ++i) {
			glGenBuffers(1, &_culledVbo[i]);
			glBindBuffer(GL_ARRAY_BUFFER, _culledVbo[i]);
			glBufferData(GL_ARRAY_BUFFER, _positionsVec3.size() * sizeof(float) * 3, 0, GL_DYNAMIC_COPY);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
		}
		//----------------------------
		//������� ���������� ����� � ����������� � ���� ����� ��� ������������ ��� ������� ����������
		for (int i = 0; i < 3; ++i) {
			_bufferTex[i] = std::make_shared<Texture>(GL_TEXTURE_BUFFER);
			_bufferTex[i]->bind();
			glTexBuffer(GL_TEXTURE_BUFFER, GL_RGB32F_ARB, _culledVbo[i]);
			_bufferTex[i]->unbind();
		}

		//=========================================================
		//������������� ��������

		_shader = std::make_shared<ShaderProgram>("599BogdanovData3/textureNormalMapDirectLight.vert", "599BogdanovData3/textureNormalMapDirectLight.frag");
		_leafShader = std::make_shared<ShaderProgram>("599BogdanovData3/leaf.vert", "599BogdanovData3/leaf.frag");
		_groundShader = std::make_shared<ShaderProgram>("599BogdanovData3/ground.vert", "599BogdanovData3/ground.frag");

		_cullingShader = std::make_shared<ShaderProgram>();

		ShaderPtr cullVert = std::make_shared<Shader>(GL_VERTEX_SHADER);
		cullVert->createFromFile("599BogdanovData3/cull.vs");
		_cullingShader->attachShader(cullVert);

		ShaderPtr cullGeom = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
		cullGeom->createFromFile("599BogdanovData3/cull.gs");
		_cullingShader->attachShader(cullGeom);

		//�������� ����������, ������� ����� �������� � �����
		const char *vars[] = { "position1", "gl_NextBuffer", "position2", "gl_NextBuffer", "position3" };
		glTransformFeedbackVaryings(_cullingShader->id(), 5, vars, GL_INTERLEAVED_ATTRIBS);

		_cullingShader->linkProgram();

		glUseProgram(_cullingShader->id());
		glUniform1i(glGetUniformLocation(_cullingShader->id(), "HiZBuffer"), 0);

		_hizShader = std::make_shared<ShaderProgram>();

		ShaderPtr dummyVS = std::make_shared<Shader>(GL_VERTEX_SHADER);
		dummyVS->createFromFile("599BogdanovData3/dummy.vs");
		_hizShader->attachShader(dummyVS);

		ShaderPtr hizGS = std::make_shared<Shader>(GL_GEOMETRY_SHADER);
		hizGS->createFromFile("599BogdanovData3/hi-z.gs");
		_hizShader->attachShader(hizGS);

		ShaderPtr hizFS = std::make_shared<Shader>(GL_FRAGMENT_SHADER);
		hizFS->createFromFile("599BogdanovData3/hi-z.fs");
		_hizShader->attachShader(hizFS);

		_hizShader->linkProgram();

		glUseProgram(_hizShader->id());
		glUniform1i(glGetUniformLocation(_hizShader->id(), "LastMip"), 0);
		//=========================================================
		//������������� �������� ���������� ��������
		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
		_light.ambient = glm::vec3(0.4, 0.4, 0.4);
		_light.diffuse = glm::vec3(0.8, 0.8, 0.8);
		_light.specular = glm::vec3(0.1, 0.1, 0.1);

		//=========================================================
		//�������� � �������� �������
		_barkTexture = loadTexture("599BogdanovData3/Bark_0009_diffuse.jpg");
		_normalTexture = loadTexture("599BogdanovData3/Bark_0009_normal.jpg");
		_leafTexture = loadTexture("599BogdanovData3/leaf1.png");
		_grassTexture = loadTexture("599BogdanovData3/grass.jpg");
		_brickTexture = loadTexture("599BogdanovData3/brick.jpg");

		//=========================================================
		//������������� ��������, �������, ������� ������ ��������� ������ �� ��������
		glGenSamplers(1, &_sampler);
		glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
		
		// ������ ��� ��������� �������
		glGenTransformFeedbacks(1, &_transformFeedback);
		for (int i = 0; i < 3; ++i) {
			glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _transformFeedback);
			glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, i, _culledVbo[i]);
			glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);
		}
	

		//VAO, ������� ����� ���������� ������ ��� ���������
		glGenVertexArrays(1, &_precullVao);
		glBindVertexArray(_precullVao);
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, _bufVec3->id());
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glBindVertexArray(0);
		
		//----------------------------

		// query ����� �������� ����� ��������� �������� 
		glGenQueries(3, _culledTreeQueries);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::Text("Tree Polygons %d", _treePolygons);
			ImGui::Text("low res unoccluded trees %d", _unoccludedTrees[0]);
			ImGui::Text("med res unoccluded trees %d", _unoccludedTrees[1]);
			ImGui::Text("high res unoccluded trees %d", _unoccludedTrees[2]);

			if (ImGui::CollapsingHeader("Light"))
			{
				ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
				ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
				ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

				ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
				ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
			}
		}
		ImGui::End();
	}

	void cull(const ShaderProgramPtr& shader) {
		shader->use();
		
		glEnable(GL_RASTERIZER_DISCARD);
		
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, _transformFeedback);
		
		for (int i = 0; i < 3; i++)
			glBeginQueryIndexed(GL_PRIMITIVES_GENERATED, i, this->_culledTreeQueries[i]);
		
		glBeginTransformFeedback(GL_POINTS);
		
		glBindVertexArray(_precullVao);
		glDrawArrays(GL_POINTS, 0, _positionsVec3.size());
		
		glEndTransformFeedback();
		
		for (int i = 0; i < 3; i++)
			glEndQueryIndexed(GL_PRIMITIVES_GENERATED, i);
		
		glDisable(GL_RASTERIZER_DISCARD);
		
	}

	void setupHIZbuffers(int SCREEN_WIDTH, int SCREEN_HEIGHT) {
		// �������� ��� �������
		glGenTextures(1, &_depthTex);
		glBindTexture(GL_TEXTURE_2D, _depthTex);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, SCREEN_WIDTH, SCREEN_HEIGHT, 0,
			GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
		// ����� � ��� ������-��������
		glGenerateMipmap(GL_TEXTURE_2D);

		// ���������� ��� ���������� ����������
		glGenFramebuffers(1, &_hizFramebuffer);
		glBindFramebuffer(GL_FRAMEBUFFER, _hizFramebuffer);
		// �������� ��� �������
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthTex, 0);
	}

	void createHIZmap(int SCREEN_WIDTH, int SCREEN_HEIGHT)
	{
		glUseProgram(_hizShader->id());
		// ��������� ����, ������ ��� �����
		glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _depthTex);
		// ��������� ���� �������, �� ��������� � ������
		glDepthFunc(GL_ALWAYS);
		// ���������, ������� ������� ������������
		int numLevels = 1 + (int)floorf(log2f(fmaxf(SCREEN_WIDTH, SCREEN_HEIGHT)));
		int currentWidth = SCREEN_WIDTH;
		int currentHeight = SCREEN_HEIGHT;
		for (int i = 1; i < numLevels; i++) {
			glUniform2i(glGetUniformLocation(_hizShader->id(), "LastMipSize"), currentWidth, currentHeight);
			// ��������� ������ ���������� ����
			currentWidth /= 2;
			currentHeight /= 2;
			// ��������, ��� �� ������� ���� 1 �� 1
			currentWidth = currentWidth > 0 ? currentWidth : 1;
			currentHeight = currentHeight > 0 ? currentHeight : 1;
			glViewport(0, 0, currentWidth, currentHeight);
			// ������� ������ �� ��������� ������� � �������� ������������ ����������
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, i - 1);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, i - 1);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthTex, i);
			// ������ ������� ��������� ������ ��� �������������� ������ ��� ������ ����
			glDrawArrays(GL_POINTS, 0, 1);
		}
		// ������������� ������ ��� �������
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, numLevels - 1);
		// ���������� ������������ �����������
		//glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->colorTex, 0);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, _depthTex, 0);
		// ��������� ����, ���� ������� � ��������� �������
		glDepthFunc(GL_LEQUAL);
		glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
		glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	}

	void draw() override
	{
		//�������� ������� ������� ������ � ��������� �������
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		if (!_hizBuffersSetUp) {
			setupHIZbuffers(width, height);
			_hizBuffersSetUp = true;
		}
		// ����������� ����������� ���������� ��� ������������� z-�����
		glBindFramebuffer(GL_FRAMEBUFFER, _hizFramebuffer);
		//������� ������ ����� � ������� �� ����������� ���������� ����������� �����
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta));
		glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 0.0));
		//��������� �� ���������� �������� �������-����������
		_groundShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_groundShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_groundShader->setVec3Uniform("light.pos", lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
		_groundShader->setVec3Uniform("light.La", _light.ambient);
		_groundShader->setVec3Uniform("light.Ld", _light.diffuse);
		_groundShader->setVec3Uniform("light.Ls", _light.specular);
		// ������ ���������
		{
			// ���������� ������ ��� �����
			{
				_groundShader->use();

				GLuint textureUnitForGroundTex = 0;

				if (USE_DSA) {
					glBindTextureUnit(textureUnitForGroundTex, _grassTexture->texture());
					glBindSampler(textureUnitForGroundTex, _sampler);
				}
				else {
					glBindSampler(textureUnitForGroundTex, _sampler);
					glActiveTexture(GL_TEXTURE0 + textureUnitForGroundTex);  //���������� ���� 0
					_grassTexture->bind();
				}

				_groundShader->setIntUniform("diffuseTex", textureUnitForGroundTex);

				{
					_groundShader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
					_groundShader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));

					_ground->draw();
				}
			}
			//������� ������ ������, ����� ��� �� ������, ��� � ��� �����
			{
				GLuint textureUnitForBrickTex = 0;

				if (USE_DSA) {
					glBindTextureUnit(textureUnitForBrickTex, _brickTexture->texture());
					glBindSampler(textureUnitForBrickTex, _sampler);
				}
				else {
					glBindSampler(textureUnitForBrickTex, _sampler);
					glActiveTexture(GL_TEXTURE0 + textureUnitForBrickTex);  //���������� ���� 0
					_brickTexture->bind();
				}

				_groundShader->setIntUniform("diffuseTex", textureUnitForBrickTex);

				for (int i = 0; i < _positionsHouses.size(); ++i)
				{
					_groundShader->setMat4Uniform("modelMatrix", glm::translate(_house->modelMatrix(), _positionsHouses[i]));
					_groundShader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _house->modelMatrix()))));

					_house->draw();
				}
			}
			// �� �� ������ ������������ ����� ������
			createHIZmap(width, height);
		}
		// ������ ����� �������� � ���������� ����������
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		{
			{
				_groundShader->use();

				GLuint textureUnitForGroundTex = 0;

				if (USE_DSA) {
					glBindTextureUnit(textureUnitForGroundTex, _grassTexture->texture());
					glBindSampler(textureUnitForGroundTex, _sampler);
				}
				else {
					glBindSampler(textureUnitForGroundTex, _sampler);
					glActiveTexture(GL_TEXTURE0 + textureUnitForGroundTex);  //���������� ���� 0
					_grassTexture->bind();
				}

				_groundShader->setIntUniform("diffuseTex", textureUnitForGroundTex);

				{
					_groundShader->setMat4Uniform("modelMatrix", _ground->modelMatrix());
					_groundShader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _ground->modelMatrix()))));

					_ground->draw();
				}
			}
			//������� ������ ������, ����� ��� �� ������, ��� � ��� �����
			{
				GLuint textureUnitForBrickTex = 0;

				if (USE_DSA) {
					glBindTextureUnit(textureUnitForBrickTex, _brickTexture->texture());
					glBindSampler(textureUnitForBrickTex, _sampler);
				}
				else {
					glBindSampler(textureUnitForBrickTex, _sampler);
					glActiveTexture(GL_TEXTURE0 + textureUnitForBrickTex);  //���������� ���� 0
					_brickTexture->bind();
				}

				_groundShader->setIntUniform("diffuseTex", textureUnitForBrickTex);

				for (int i = 0; i < _positionsHouses.size(); ++i)
				{
					_groundShader->setMat4Uniform("modelMatrix", glm::translate(_house->modelMatrix(), _positionsHouses[i]));
					_groundShader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _house->modelMatrix()))));

					_house->draw();
				}
			}
		}
		//����� ������ ��� ��������� � ���������� ���
		{
			_cullingShader->use();
			_cullingShader->setMat4Uniform("ProjectionMatrix", _camera.projMatrix);
			_cullingShader->setMat4Uniform("ModelViewMatrix", _camera.viewMatrix);
			_cullingShader->setVec3Uniform("ObjectExtent", _boundingRect);
			_cullingShader->setVec4Uniform("Viewport", glm::vec4(0.0, 0.0, width, height));
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, _depthTex);
			_cullingShader->setIntUniform("HiZBuffer", 0);

			cull(_cullingShader);
		}
		GLuint visibleObjects[3];
		for (int i = 0; i < 3; ++i)
			glGetQueryObjectuiv(_culledTreeQueries[i], GL_QUERY_RESULT, &visibleObjects[i]);
		{
			//���������� ������		
			_shader->use();

			//��������� �� ���������� �������� �������-����������
			_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

			_shader->setVec3Uniform("light.pos", lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
			_shader->setVec3Uniform("light.La", _light.ambient);
			_shader->setVec3Uniform("light.Ld", _light.diffuse);
			_shader->setVec3Uniform("light.Ls", _light.specular);

			GLuint textureUnitForDiffuseTex = 0;

			if (USE_DSA) {
				glBindTextureUnit(textureUnitForDiffuseTex, _barkTexture->texture());
				glBindSampler(textureUnitForDiffuseTex, _sampler);
			}
			else {
				glBindSampler(textureUnitForDiffuseTex, _sampler);
				glActiveTexture(GL_TEXTURE0 + textureUnitForDiffuseTex);  //���������� ���� 0
				_barkTexture->bind();
			}

			GLuint textureUnitForNormalTex = 1;

			if (USE_DSA) {
				glBindTextureUnit(textureUnitForNormalTex, _normalTexture->texture());
				glBindSampler(textureUnitForNormalTex, _sampler);
			}
			else {
				glBindSampler(textureUnitForNormalTex, _sampler);
				glActiveTexture(GL_TEXTURE0 + textureUnitForNormalTex);  //���������� ���� 1
				_normalTexture->bind();
			}

			_shader->setIntUniform("diffuseTex", textureUnitForDiffuseTex);
			_shader->setIntUniform("normalTex", textureUnitForNormalTex);

			//��������� �� ���������� ������� ������ ����� � ��������� ���������

			_treePolygons = 0;
			for (int j = 0; j < 3; ++j) {
				glm::vec3 tmp = _light.ambient;
				tmp[j] += 0.2;
				_shader->setVec3Uniform("light.La", tmp);
				glActiveTexture(GL_TEXTURE2);
				_bufferTex[j]->bind();
				_shader->setIntUniform("texBuf", 2);
				_unoccludedTrees[j] = visibleObjects[j];
				for (int i = 0; i < _cylinders[j].size(); ++i) {
					_shader->setMat4Uniform("modelMatrix", _cylinders[j][i]->modelMatrix());
					_shader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix *
						_cylinders[j][i]->modelMatrix()))));
					_treePolygons += _cylinders[j][i]->getTrianglesCount() * visibleObjects[j];

					_cylinders[j][i]->drawInstanced(visibleObjects[j]);
				}
			}
		}
		{
			// ���������� ������ ��� �������
			_leafShader->use();

			//��������� �� ���������� �������� �������-����������
			_leafShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
			_leafShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

			_leafShader->setVec3Uniform("light.pos", lightPosCamSpace); //�������� ��������� ��� � ������� ����������� ������
			_leafShader->setVec3Uniform("light.La", _light.ambient);
			_leafShader->setVec3Uniform("light.Ld", _light.diffuse);
			_leafShader->setVec3Uniform("light.Ls", _light.specular);

			GLuint textureUnitForLeafTex = 0;

			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glEnable(GL_POLYGON_OFFSET_FILL);
			glPolygonOffset(1.0f, 1.0f);

			if (USE_DSA) {
				glBindTextureUnit(textureUnitForLeafTex, _leafTexture->texture());
				glBindSampler(textureUnitForLeafTex, _sampler);
			}
			else {
				glBindSampler(textureUnitForLeafTex, _sampler);
				glActiveTexture(GL_TEXTURE0 + textureUnitForLeafTex);  //���������� ���� 0
				_leafTexture->bind();
			}

			_leafShader->setIntUniform("diffuseTex", textureUnitForLeafTex);
			for (int j = 0; j < 3; ++j) {
				glActiveTexture(GL_TEXTURE1);
				_bufferTex[j]->bind();
				_leafShader->setIntUniform("texBuf", 1);
				for (int i = 0; i < _leaves.size(); ++i) {
					_leafShader->setMat4Uniform("modelMatrix", _leaves[i]->modelMatrix());
					_leafShader->setMat3Uniform("localToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _leaves[i]->modelMatrix()))));

					_leaves[i]->drawInstanced(visibleObjects[j]);
				}
			}
		}
		//����������� ������� � ��������� ���������
		glBindSampler(0, 0);
		glUseProgram(0);
	}
};

int main()
{
	TestingApplication app;
	app.start();

	return 0;
}