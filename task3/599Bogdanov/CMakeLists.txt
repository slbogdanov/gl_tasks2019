file(GLOB_RECURSE SRC_FILES ${CMAKE_CURRENT_SOURCE_DIR} *.cpp *.hpp *.c *.cpp)
MAKE_OPENGL_TASK(599Bogdanov 3 "${SRC_FILES}")

set(COMMON_DIR ${CMAKE_CURRENT_SOURCE_DIR}/common)
target_include_directories(599Bogdanov3 PUBLIC ${COMMON_DIR})

if (UNIX)
    target_link_libraries(599Bogdanov3 stdc++fs)
endif()