/**
Преобразует координаты вершины и нормально в систему координат виртуальной камеры и передает на выход.
Копирует на выход текстурные координаты.
*/

#version 430 core

uniform samplerBuffer texBuf;

//стандартные матрицы для преобразования координат
uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

//матрица для преобразования из локальной системы координат в систему координат камеры
uniform mat3 localToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины
layout(location = 3) in vec3 vertexTangent; //касательная в локальной системе координат
layout(location = 4) in vec3 vertexBitangent; //бикасательная в локальной системе координат

out vec3 normalCamSpace; //нормаль в системе координат камеры
out vec3 tangentCamSpace; //касательная в системе координат камеры
out vec3 bitangentCamSpace; //бикасательная в системе координат камеры
out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec2 texCoord; //текстурные координаты

void main()
{
	vec3 modelPos = texelFetch(texBuf, gl_InstanceID).rgb;
	
	texCoord = vertexTexCoord;

	posCamSpace = viewMatrix * (modelMatrix * vec4(vertexPosition, 1.0) + vec4(modelPos, 1.0)); //преобразование координат вершины в систему координат камеры
	normalCamSpace = normalize(localToCameraMatrix * vertexNormal); //преобразование нормали в систему координат камеры
	tangentCamSpace = normalize(localToCameraMatrix * vertexTangent); //преобразование касательной в систему координат камеры
	bitangentCamSpace = normalize(localToCameraMatrix * vertexBitangent); //преобразование бикасательной в систему координат камеры
	
	gl_Position = projectionMatrix * viewMatrix * (modelMatrix * vec4(vertexPosition, 1.0) + vec4(modelPos, 1.0));
}
