#version 400 core

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform vec4 Viewport;

uniform vec3 ObjectExtent;

uniform sampler2D HiZBuffer;

layout(location = 0) in vec3 position;

out vec3 positionOrig;
out int objectRes;
flat out int objectVisible;

void main(void) {

   positionOrig = position;
	
   gl_Position = vec4(position, 1.0);
   
   /* calculate modelview projection matrix */
   mat4 MVP = ProjectionMatrix * ModelViewMatrix;
   
   /* create the bounding box of the object */
   vec4 BoundingBox[8];
   BoundingBox[0] = MVP * ( gl_Position + vec4( ObjectExtent.x, ObjectExtent.y, 2.0 * ObjectExtent.z, 1.0) );
   BoundingBox[1] = MVP * ( gl_Position + vec4(-ObjectExtent.x, ObjectExtent.y, 2.0 * ObjectExtent.z, 1.0) );
   BoundingBox[2] = MVP * ( gl_Position + vec4( ObjectExtent.x,-ObjectExtent.y, 2.0 * ObjectExtent.z, 1.0) );
   BoundingBox[3] = MVP * ( gl_Position + vec4(-ObjectExtent.x,-ObjectExtent.y, 2.0 * ObjectExtent.z, 1.0) );
   BoundingBox[4] = MVP * ( gl_Position + vec4( ObjectExtent.x, ObjectExtent.y, 0.1, 1.0) );
   BoundingBox[5] = MVP * ( gl_Position + vec4(-ObjectExtent.x, ObjectExtent.y, 0.1, 1.0) );
   BoundingBox[6] = MVP * ( gl_Position + vec4( ObjectExtent.x,-ObjectExtent.y, 0.1, 1.0) );
   BoundingBox[7] = MVP * ( gl_Position + vec4(-ObjectExtent.x,-ObjectExtent.y, 0.1, 1.0) );
   
   /* check how the bounding box resides regarding to the view frustum */   
   int outOfBound[6] = int[6]( 0, 0, 0, 0, 0, 0 );

   for (int i=0; i<8; i++)
   {
      if ( BoundingBox[i].x >  BoundingBox[i].w ) outOfBound[0]++;
      if ( BoundingBox[i].x < -BoundingBox[i].w ) outOfBound[1]++;
      if ( BoundingBox[i].y >  BoundingBox[i].w ) outOfBound[2]++;
      if ( BoundingBox[i].y < -BoundingBox[i].w ) outOfBound[3]++;
      if ( BoundingBox[i].z >  BoundingBox[i].w ) outOfBound[4]++;
      if ( BoundingBox[i].z < -BoundingBox[i].w ) outOfBound[5]++;
   }

   bool inFrustum = true;
   
   for (int i=0; i<6; i++)
      if ( outOfBound[i] == 8 ) {
         inFrustum = false;
      }

   objectVisible = inFrustum ? 1 : 0;

   if (objectVisible == 1) {

	   for (int i=0; i<8; i++)
			BoundingBox[i].xyz /= BoundingBox[i].w;

	   /* calculate screen space bounding rectangle */
	   vec2 BoundingRect[2];
		BoundingRect[0].x = min( min( min( BoundingBox[0].x, BoundingBox[1].x ),
									  min( BoundingBox[2].x, BoundingBox[3].x ) ),
								 min( min( BoundingBox[4].x, BoundingBox[5].x ),
									  min( BoundingBox[6].x, BoundingBox[7].x ) ) ) / 2.0 + 0.5;
		BoundingRect[0].y = min( min( min( BoundingBox[0].y, BoundingBox[1].y ),
									  min( BoundingBox[2].y, BoundingBox[3].y ) ),
								 min( min( BoundingBox[4].y, BoundingBox[5].y ),
									  min( BoundingBox[6].y, BoundingBox[7].y ) ) ) / 2.0 + 0.5;
		BoundingRect[1].x = max( max( max( BoundingBox[0].x, BoundingBox[1].x ),
									  max( BoundingBox[2].x, BoundingBox[3].x ) ),
								 max( max( BoundingBox[4].x, BoundingBox[5].x ),
									  max( BoundingBox[6].x, BoundingBox[7].x ) ) ) / 2.0 + 0.5;
		BoundingRect[1].y = max( max( max( BoundingBox[0].y, BoundingBox[1].y ),
									  max( BoundingBox[2].y, BoundingBox[3].y ) ),
								 max( max( BoundingBox[4].y, BoundingBox[5].y ),
									  max( BoundingBox[6].y, BoundingBox[7].y ) ) ) / 2.0 + 0.5;
		/* then the linear depth value of the front-most point */
		float InstanceDepth = min( min( min( BoundingBox[0].z, BoundingBox[1].z ),
										min( BoundingBox[2].z, BoundingBox[3].z ) ),
								   min( min( BoundingBox[4].z, BoundingBox[5].z ),
										min( BoundingBox[6].z, BoundingBox[7].z ) ) );

	   /* now we calculate the bounding rectangle size in viewport coordinates */
		float ViewSizeX = (BoundingRect[1].x-BoundingRect[0].x) * Viewport.y;
		float ViewSizeY = (BoundingRect[1].y-BoundingRect[0].y) * Viewport.z;
	
		/* now we calculate the texture LOD used for lookup in the depth buffer texture */
		float LOD = ceil( log2( max( ViewSizeX, ViewSizeY ) / 2.0 ) );

		/* finally fetch the depth texture using explicit LOD lookups */
		vec4 Samples;
		Samples.x = textureLod( HiZBuffer, vec2(BoundingRect[0].x, BoundingRect[0].y), LOD ).x;
		Samples.y = textureLod( HiZBuffer, vec2(BoundingRect[0].x, BoundingRect[1].y), LOD ).x;
		Samples.z = textureLod( HiZBuffer, vec2(BoundingRect[1].x, BoundingRect[1].y), LOD ).x;
		Samples.w = textureLod( HiZBuffer, vec2(BoundingRect[1].x, BoundingRect[0].y), LOD ).x;
		float MaxDepth = max( max( Samples.x, Samples.y ), max( Samples.z, Samples.w ) );

		int notOccluded = ( InstanceDepth > MaxDepth ) ? 0 : 1;

		if (notOccluded == 0) objectVisible = 0;
	}

   vec4 centerPos = gl_Position;
   centerPos[2] += ObjectExtent.z;
   //vec4 centerInCamera = (ModelViewMatrix * centerPos);
   float cameraDist = (ModelViewMatrix * centerPos).z * -1;
   if (cameraDist < 1.5) objectRes = 2;
   else if (cameraDist >= 1.5 && cameraDist < 4.5) objectRes = 1;
   else objectRes = 0;
}
