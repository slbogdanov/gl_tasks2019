#version 400 core

layout(points) in;
layout(points, max_vertices = 1 ) out;

in vec3 positionOrig[1];
in int objectRes[1];
flat in int objectVisible[1];

layout(stream=0) out vec3 position1;
layout(stream=1) out vec3 position2;
layout(stream=2) out vec3 position3;

void main() {

   /* only emit primitive if the object is visible */
   if ( objectVisible[0] == 1 )
   {
      position1 = positionOrig[0];
	  position2 = positionOrig[0];
	  position3 = positionOrig[0];
	  if (objectRes[0] == 0) EmitStreamVertex(0);
	  if (objectRes[0] == 1) EmitStreamVertex(1);
	  if (objectRes[0] == 2) EmitStreamVertex(2);
      //EndPrimitive();
   }
}
